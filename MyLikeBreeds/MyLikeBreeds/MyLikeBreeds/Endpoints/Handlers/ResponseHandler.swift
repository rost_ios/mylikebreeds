//
//  ResponseHandler.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import Realm


class ResponseHandler {
        
    /// ---> Fucntion for handle recieved data and send results to back in view controller  <--- ///
    static func handleData(_ sender: Data, by type: ResponseDataTypes) {
        do {
            let json = try JSONSerialization.jsonObject(with: sender, options: .allowFragments) as? [String]
            
            if let array = json {
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: getNotificationName(type),
                                                    object: nil,
                                                    userInfo: ["data": array])
                }
            } else {
                handleError("Serialization error", by: type)
            }
        } catch let error {
            handleError(error.localizedDescription, by: type)
        }
    }

    
    /// ---> Function for pass back error to alert in view controller <--- ///
    static func handleError(_ error: String, by type: ResponseDataTypes) {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: getNotificationName(type),
                                            object: nil,
                                            userInfo: ["error": error])
        }
    }
    
    
    /// ---> Function for get notification name by type of request <--- ///
    static func getNotificationName(_ type: ResponseDataTypes) -> Notification.Name {
        switch type {
            case .breeds:
                return getBreedsResult
            case .images:
                return getImagesResult
        }
    }
}
