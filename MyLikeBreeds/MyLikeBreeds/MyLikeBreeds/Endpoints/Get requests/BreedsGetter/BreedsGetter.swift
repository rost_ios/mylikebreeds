//
//  BreedsGetter.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import Alamofire


class BreedsGetter {
        
    /// ---> Function for get list of all breeds <--- ///
    static func getAllBreeds() {
        let requestUrl = apiUrl + "/" + breedParam
        
        AF.request(requestUrl, method: .get, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
                case .success(_):
                    if let breeds = response.data {
                        ResponseHandler.handleData(breeds, by: .breeds)
                    } else {
                        ResponseHandler.handleError("You received empty date in breeds response", by: .breeds)
                    }
                case .failure(let error):                    ResponseHandler.handleError(error.localizedDescription, by: .breeds)
            }
        }
    }
}
