//
//  ImagesGetter.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import Alamofire


class ImagesGetter {
        
    /// ---> Function for get list of images for selected breed <--- ///
    static func getImages() {
        if let breed = DataContainer.shared.selectedBreed {
            let requestUrl = apiUrl + "/" + breed + "/" + imagesParam
            
            AF.request(requestUrl, method: .get, encoding: JSONEncoding.default).responseJSON { response in
                switch response.result {
                    case .success(_):
                        if let images = response.data {
                            ResponseHandler.handleData(images, by: .images)
                        } else {
                            ResponseHandler.handleError("You received empty date in images response", by: .images)
                        }
                    case .failure(let error):
                        ResponseHandler.handleError(error.localizedDescription, by: .images)
                }
            }
        }
    }
}
