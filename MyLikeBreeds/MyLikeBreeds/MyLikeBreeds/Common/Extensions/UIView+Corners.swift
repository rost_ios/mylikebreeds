//
//  UIView+Corners.swift
//  MyLikeBreeds
//
//  Created by Rost on 08.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


extension UIView {
    
    /// ---> Function round corners on view and add border <--- ///
    func roundCorners(_ radius: CGFloat, border: CGFloat) {
        layer.cornerRadius = radius
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        if border > 0 {
            layer.borderWidth = border
            layer.borderColor = UIColor.gray.cgColor
        }
        
        layer.masksToBounds = true
    }
}
