//
//  Results+Array.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import RealmSwift


extension Results {
    
    /// ---> Function convert realm results array into usual array <--- ///
    func toArray<T>(_ ofType: T.Type) -> [T] {
        var array = [T]()
        for i in 0 ..< count {
            if let result = self[i] as? T {
                array.append(result)
            }
        }

        return array
    }
}
