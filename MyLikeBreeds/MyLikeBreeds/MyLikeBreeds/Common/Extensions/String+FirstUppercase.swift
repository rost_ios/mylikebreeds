//
//  String+FirstUppercase.swift
//  MyLikeBreeds
//
//  Created by Rost on 08.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


extension String {
    
    /// ---> Function for uppercase first character in string <--- ///
    func upFirstChar() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func upFirstChar() {
        self = self.upFirstChar()
    }
}
