//
//  UIBuilder.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class UIBuilder {
            
    /// ---> Function for show alert for some view controller <--- ///
    static func showAlert(_ text: String, in vc: UIViewController) {
        let alert = UIAlertController(title: "Alert!", message: text, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        vc.present(alert, animated: true, completion: nil)
    }
}

