//
//  Enums.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


enum ResponseDataTypes: Int {
    case breeds          = 0
    case images
}
