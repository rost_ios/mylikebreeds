//
//  Constants.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


/// ---> Breeds API urls and params <--- ///
let apiUrl      = "https://hidden-crag-71735.herokuapp.com/api"
let breedParam  = "breeds"
let imagesParam = "images"


/// ---> Observers names <--- ///
let getBreedsResult        = Notification.Name("getBreedsResult")
let getImagesResult        = Notification.Name("getImagesResult")


/// ---> Placeholders <--- ///
let placeholderImage        = UIImage(named: "dog_image_holder")
let favPaceholderImage      = UIImage(named: "fav_image_holder")
