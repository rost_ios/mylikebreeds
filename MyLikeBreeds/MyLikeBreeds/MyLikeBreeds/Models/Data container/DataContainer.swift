//
//  DataContainer.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


class DataContainer {
    static let shared = DataContainer()
    private init() { }
    
    var selectedBreed: String?     // title of selected breed for images view controller    
}
