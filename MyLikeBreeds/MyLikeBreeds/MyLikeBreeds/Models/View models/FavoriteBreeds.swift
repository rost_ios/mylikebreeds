//
//  FavoriteBreeds.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import Realm


class FavoriteBreeds {
        
    /// ---> Function for save/remove breed to/from local storage <--- ///
    static func saveFavoriteBreed(_ title: String, images: [String]) {
        var firstImages = [String]()
        if images.count >= 5 {
             firstImages = Array(images.prefix(5))
        } else {
            firstImages = Array(images.prefix(images.count))
        }
                
        try! uiRealm.write {
            if !BreedObject.isBreedExists(title) {
                let favoriteBreed = BreedObject(title, images: firstImages)
                uiRealm.add(favoriteBreed)
            } else {
                BreedObject.deleteBreed(title)
            }
        }
    }
    
    
    /// ---> Function for get all saved breeds from local storage <--- ///
    static func allFavoriteBreeds() -> [BreedObject]? {
        if !uiRealm.isEmpty {
            let breeds = uiRealm.objects(BreedObject.self).toArray(BreedObject.self)

            return breeds
        }
        
        return nil
    }
    
    
    /// ---> Function for make status image in favorite button <--- ///
    static func favoriteImage(_ title: String) -> String {
        var favImage = "favorite_button_disabled"
        
        let isExist = BreedObject.isBreedExists(title)
        if isExist {
            favImage = "favorite_button_enabled"
        }
        
        return favImage
    }
}
