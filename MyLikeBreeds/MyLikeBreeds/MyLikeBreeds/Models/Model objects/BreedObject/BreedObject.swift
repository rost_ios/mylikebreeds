//
//  BreedObject.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import Realm
import RealmSwift


class BreedObject: Object {
    @objc dynamic var breedTitle: String           = ""
    let breedImages  = List<String>()
            
    /// ---> Funciton for make primary key of new object  <--- ///
    override static func primaryKey() -> String? {
        return "breedTitle"
    }
    
    
    /// ---> Constructor with binding object varyables  <--- ///
    convenience init(_ title: String, images: [String]) {
        self.init()
        
        breedTitle  = title

        breedImages.append(objectsIn: images)
    }
    
      
    /// ---> Required constructor <--- ///
    required init() {
        super.init()
    }
    
    
    /// ---> Required constructor <--- ///
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    
    /// ---> Required constructor <--- ///
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    
    /// ---> Get usual array of urls to images <--- ///
    func imagesArray() -> [String] {
        let resultArray = Array(breedImages)
        return resultArray
    }
    
    
    /// ---> Function for check if breed is exists in local storage <--- ///
    static func isBreedExists(_ title: String) -> Bool {
        let realm = try! Realm()
        if !realm.isEmpty {
            let breed = realm.objects(BreedObject.self).filter("breedTitle == '\(title)'").first
            
            if breed != nil {
                return true
            }
        }
        
        return false
    }
    
    
    /// ---> Function for remove saved breed from local storage <--- ///
    static func deleteBreed(_ title: String) {
        let realm = try! Realm()
        if !realm.isEmpty {
            if let breed = realm.objects(BreedObject.self).filter("breedTitle == '\(title)'").first {
                realm.delete(breed)
            }
        }
    }
}
