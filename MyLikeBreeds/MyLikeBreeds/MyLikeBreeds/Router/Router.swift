//
//  Router.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class Router {
    
    /// ---> Show images view controller <--- ///
    static func showImages(_ nc: UINavigationController) {
        pushNext("Images", in: nc)
    }
    
    
    /// ---> Show favorites view controller <--- ///
    static func showFavorites(_ nc: UINavigationController) {
        pushNext("Favorites", in: nc)
    }
    
    
    /// ---> Push next view controller <--- ///
    static func pushNext(_ name: String, in nc: UINavigationController) {
        let nextStoryboard = UIStoryboard(name: name, bundle: nil)
        let nextViewController = nextStoryboard.instantiateViewController(withIdentifier: name + "ViewController")
        nc.pushViewController(nextViewController, animated: true)
    }
}

