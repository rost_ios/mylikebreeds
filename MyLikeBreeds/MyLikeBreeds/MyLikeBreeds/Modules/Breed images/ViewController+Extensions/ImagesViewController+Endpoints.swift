//
//  ImagesViewController+Endpoints.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


extension ImagesViewController {
        
    /// ---> Function for check Interntet connection <--- ///
    func checkNetworkConnection() {
        reachability.whenReachable = { [weak self] reachability in
            guard let strongSelf = self else {
              return
            }
            
            if reachability.connection == .wifi || reachability.connection == .cellular  {
                strongSelf.loadAllImages()
            } else {
                strongSelf.showConnectionError()
            }
        }
        
        reachability.whenUnreachable = { [weak self] _ in
            guard let strongSelf = self else {
              return
            }
            
            strongSelf.showConnectionError()
        }

        do {
            try reachability.startNotifier()
        } catch {
            UIBuilder.showAlert("Unable to start notifier", in: self)
        }
    }
    
    
    /// ---> Function for show error about absent Interntet connection <--- ///
    func showConnectionError() {
        UIBuilder.showAlert("Internet connection is unavailable now. Please try to get breeds a later.",
                            in: self)
    }
    
    
    /// ---> Function for download images by selected breed <--- ///
    func loadAllImages() {
        DispatchQueue.global().async {
            ImagesGetter.getImages()
        }
    }
}
