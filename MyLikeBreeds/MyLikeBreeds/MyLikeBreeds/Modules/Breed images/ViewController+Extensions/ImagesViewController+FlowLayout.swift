//
//  ImagesViewController+FlowLayout.swift
//  MyLikeBreeds
//
//  Created by Rost on 08.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


extension ImagesViewController: UICollectionViewDelegateFlowLayout {

    /// ---> Function for set dynamically width and height of cell   <--- ///
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellSize, height: cellSize)
    }
    
    
    /// ---> Function for set cell indents   <--- ///
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return cellMargin
    }

    
    /// ---> Function for set section indent   <--- ///
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return cellMargin * 2
    }

    
    /// ---> Function for set section insets   <--- ///
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: cellMargin, left: cellMargin, bottom: cellMargin, right: cellMargin)
    }
}
