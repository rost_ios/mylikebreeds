//
//  ImagesViewController+Observers.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit
import Reachability


extension ImagesViewController {
        
    /// ---> Function for add observers to notification center  <--- ///
    func addObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector:    #selector(refreshImages),
                                               name:        getImagesResult,
                                               object:      nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reachabilityChanged(note:)),
                                               name: .reachabilityChanged,
                                               object: reachability)
    }
    
    
    /// ---> Function for remove all observers from notification center  <--- ///
    func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    /// ---> Function for refresh breeds table  <--- ///
    @objc func refreshImages(_ sender: Notification) {
        if let userInfo = sender.userInfo as? [String: Any] {
            if let array = userInfo["data"] as? [String] {
                imagesArray = array
                imagesCollectionView.reloadData()
            } else if let error = userInfo["error"] as? String {
                UIBuilder.showAlert(error, in: self)
            }
        }
    }
    
    
    /// ---> Function for change dataSource based on network availability  <--- ///
    @objc func reachabilityChanged(note: Notification) {
        
        let reachability = note.object as! Reachability
        
        switch reachability.connection {
            case .wifi, .cellular:
                loadAllImages()
            case .none:
                showConnectionError()
        }
    }
}
