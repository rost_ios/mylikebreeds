//
//  ImagesViewController+Functions.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


extension ImagesViewController {
        
    /// ---> Function for UI customization <--- ///
    func setupUI() {
        if let breed = DataContainer.shared.selectedBreed {
            title    = breed.upFirstChar() + " images"
            
            addFavoriteItem(breed)
        }

        cellSize    = 90.0
        cellMargin  = 5.0
    }
    
    
    /// ---> Function for add favorite item into navigation bar <--- ///
    func addFavoriteItem(_ title: String) {
        let imageName = FavoriteBreeds.favoriteImage(title)
        
        favButton = UIButton(type: .custom)
        favButton.setImage(UIImage(named: imageName), for: .normal)
        favButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        favButton.addTarget(self, action: #selector(favoriteItemTapped), for: .touchUpInside)
        let favItem = UIBarButtonItem(customView: favButton)

        self.navigationItem.setRightBarButtonItems([favItem], animated: true)
    }
}
