//
//  ImagesViewController+Actions.swift
//  MyLikeBreeds
//
//  Created by Rost on 08.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


extension ImagesViewController {
        
    /// ---> Favorite item handler <--- ///
    @objc func favoriteItemTapped() {
        if let breed = DataContainer.shared.selectedBreed {
            FavoriteBreeds.saveFavoriteBreed(breed, images: imagesArray)
            
            let imageName = FavoriteBreeds.favoriteImage(breed)
            favButton.setImage(UIImage(named: imageName), for: .normal)
        }
    }
}
