//
//  ImagesViewController+CollectionView.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


extension ImagesViewController: UICollectionViewDataSource {
    
    /// ---> Collection view data source fucntions <--- ///
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArray.count
    }

    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as? ImageCell
        
        let imageUrl = imagesArray[indexPath.item]
        cell?.setupUI()        
        cell?.setValues(imageUrl)
        
        return cell ?? UICollectionViewCell()
    }
}
