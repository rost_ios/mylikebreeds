//
//  ImagesViewController.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit
import Reachability


class ImagesViewController: UIViewController {
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    let reachability = Reachability()!
    var imagesArray: [String] = []
    var cellMargin: CGFloat!
    var cellSize: CGFloat!
    var favButton: UIButton!
            
    /// ---> View life cycle  <--- ///
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        addObservers()
        
        checkNetworkConnection()
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        removeObservers()
        reachability.stopNotifier()
    }    
}
