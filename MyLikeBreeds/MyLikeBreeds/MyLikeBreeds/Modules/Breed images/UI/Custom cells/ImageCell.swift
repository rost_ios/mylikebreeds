//
//  ImageCell.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class ImageCell: UICollectionViewCell {
    @IBOutlet weak var breedImageView: AsyncImageView!
    
    /// ---> Function for add UI customizations <--- ///
    func setupUI() {
        contentView.roundCorners(5, border: 0.1)
    }
    
    
    /// ---> Function for set values inside cell UI <--- ///
    func setValues(_ url: String) {
        if !url.isEmpty {
            breedImageView.loadAsync(url, placeholder: placeholderImage)
        }
    }
    
    
    /// ---> Function set cell by fixed size <--- ///
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes)
        -> UICollectionViewLayoutAttributes {
      return layoutAttributes
    }
}
