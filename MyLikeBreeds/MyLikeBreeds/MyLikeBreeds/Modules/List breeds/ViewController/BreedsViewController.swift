//
//  BreedsViewController.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import UIKit
import Reachability


class BreedsViewController: UIViewController {
    @IBOutlet weak var breedsTable: UITableView!
    var breedsArray: [String] = []
    let reachability = Reachability()!
        
    /// ---> View life cycle  <--- ///
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        addObservers()
        
        checkNetworkConnection()
    }

    
    /// ---> Destructor  <--- ///
    deinit {
        removeObservers()
        reachability.stopNotifier()
    }

}

