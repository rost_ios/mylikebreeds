//
//  BreedCell.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class BreedCell: UITableViewCell {
    @IBOutlet weak var holderImage: UIImageView!
    @IBOutlet weak var breedTitle: UILabel!
    
    /// ---> Function for add UI customizations <--- ///
    func setupUI() {
        holderImage.roundCorners(holderImage.bounds.size.width / 2.0,
                                 border: 0.2)
        
        holderImage.image = placeholderImage
    }
    
    
    /// ---> Function for set values inside cell UI <--- ///
    func setValues(_ title: String) {
        breedTitle.text     = title.upFirstChar()
    }
}
