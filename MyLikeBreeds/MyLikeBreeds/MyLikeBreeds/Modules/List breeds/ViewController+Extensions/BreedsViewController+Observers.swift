//
//  BreedsViewController+Observers.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import Reachability


extension BreedsViewController {
        
    /// ---> Function for add observers to notification center  <--- ///
    func addObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector:    #selector(refreshBreeds),
                                               name:        getBreedsResult,
                                               object:      nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
    }
    
    
    /// ---> Function for remove all observers from notification center  <--- ///
    func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    /// ---> Function for refresh breeds table  <--- ///
    @objc func refreshBreeds(_ sender: Notification) {
        if let userInfo = sender.userInfo as? [String: Any] {
            if let breeds = userInfo["data"] as? [String] {
                breedsArray = breeds
                breedsTable.reloadData()
            } else if let error = userInfo["error"] as? String {
                UIBuilder.showAlert(error, in: self)
            }
        }
    }
    
    
    /// ---> Function for change dataSource based on network availability  <--- ///
    @objc func reachabilityChanged(note: Notification) {
        
        let reachability = note.object as! Reachability
        
        switch reachability.connection {
            case .wifi, .cellular:
                loadAllBreeds()
            case .none:
                showConnectionError()
        }
    }
}
