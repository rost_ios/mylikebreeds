//
//  BreedsViewController+Functions.swift
//  MyLikeBreeds
//
//  Created by Rost on 07.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


extension BreedsViewController {
        
    /// ---> Function for UI customization <--- ///
    func setupUI() {
        title = "Home"
        
        addFavoriteItem()
        
        breedsTable.rowHeight = 68.0
        breedsTable.tableFooterView = UIView()
    }
    
    
    /// ---> Function for add favorite item to navigation bar <--- ///
    func addFavoriteItem() {
        let favButton = UIButton(type: .custom)
        favButton.setImage(UIImage(named: "favorite_list_button"), for: .normal)
        favButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        favButton.addTarget(self, action: #selector(favoriteItemTapped), for: .touchUpInside)
        let favItem = UIBarButtonItem(customView: favButton)

        self.navigationItem.setRightBarButtonItems([favItem], animated: true)
    }
    
    
    /// ---> Function for handle user selection in the table <--- ///
    func handleSelectedBreed(_ index: IndexPath) {
        let selectedTitle = breedsArray[index.row]
        DataContainer.shared.selectedBreed = selectedTitle
        
        if let nc = self.navigationController {
            Router.showImages(nc)
        }
    }
}
