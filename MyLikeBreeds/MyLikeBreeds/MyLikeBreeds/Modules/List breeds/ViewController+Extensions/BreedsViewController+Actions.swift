//
//  BreedsViewController+Actions.swift
//  MyLikeBreeds
//
//  Created by Rost on 08.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


extension BreedsViewController {
        
    /// ---> Favorite item handler <--- ///
    @objc func favoriteItemTapped() {
        if let nc = self.navigationController {
            Router.showFavorites(nc)
        }
    }
}
