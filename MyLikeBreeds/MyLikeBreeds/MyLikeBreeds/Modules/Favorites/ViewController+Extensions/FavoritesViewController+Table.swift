//
//  FavoritesViewController+Table.swift
//  MyLikeBreeds
//
//  Created by Rost on 08.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


extension FavoritesViewController: UITableViewDataSource {
        
    /// ---> Table view data source methods <--- ///
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavoriteCell", for: indexPath) as? FavoriteCell
        
        let breedObject = favsArray[indexPath.row]
        
        cell?.setupUI()
        cell?.setValues(breedObject)

        return cell ?? BreedCell()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favsArray.count
    }
}
