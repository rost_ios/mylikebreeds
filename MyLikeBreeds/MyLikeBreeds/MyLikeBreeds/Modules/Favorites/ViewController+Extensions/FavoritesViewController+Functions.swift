//
//  FavoritesViewController+Functions.swift
//  MyLikeBreeds
//
//  Created by Rost on 08.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


extension FavoritesViewController {
    
    /// ---> Function for UI customization <--- ///
    func setupUI() {
        title = "Favorites"
        
        favsTable.rowHeight = 186.0
        favsTable.tableFooterView = UIView()
    }
    
    
    /// ---> Function for refresh data and table <--- ///
    func refreshFavorites() {
        if let favorites = FavoriteBreeds.allFavoriteBreeds() {
            favsArray = favorites
            favsTable.reloadData()
        }
    }
}
