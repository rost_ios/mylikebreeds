//
//  FavoriteCell.swift
//  MyLikeBreeds
//
//  Created by Rost on 08.11.2019.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class FavoriteCell: UITableViewCell {
    @IBOutlet weak var holderImage: UIImageView!
    @IBOutlet weak var breedTitle: UILabel!
    @IBOutlet weak var firstImage: AsyncImageView!
    @IBOutlet weak var secondImage: AsyncImageView!
    @IBOutlet weak var thirdImage: AsyncImageView!
    @IBOutlet weak var fourImage: AsyncImageView!
    @IBOutlet weak var fiveImage: AsyncImageView!
    
    /// ---> Function for add UI customizations <--- ///
    func setupUI() {
        holderImage.roundCorners(holderImage.bounds.size.width / 2.0,
                                 border: 0.2)
        
        holderImage.image = favPaceholderImage
    }
    
    
    /// ---> Function for set values inside cell UI <--- ///
    func setValues(_ object: BreedObject) {
        breedTitle.text     = object.breedTitle.upFirstChar()
        
        let imagesArray = object.imagesArray()
        if imagesArray.count > 0 {
            let imageViews = [firstImage, secondImage, thirdImage, fourImage, fiveImage]
            for i in 0 ..< imagesArray.count {
                if let view   = imageViews[i] {
                    let imageUrl    = imagesArray[i]
                    view.loadAsync(imageUrl, placeholder: placeholderImage)
                }
            }
        }
    }
}
